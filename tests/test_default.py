import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('all')


def test_ess_services(File):
    install_root = '/export/nfsroots/centos7/rootfs'
    for script in ('ess_boot.py', 'procServ_vacuum.py', 'procServ'):
        assert File(install_root + '/usr/bin/' + script).exists
    for unit in ('ess-boot.service', 'ioc@.service', 'procServ-vacuum.service'):
        assert File(install_root + '/etc/systemd/system/' + unit).exists
